from typing import Text, List, Any, Dict

from rasa_sdk import Tracker, FormValidationAction
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.types import DomainDict

from .actions_general_quiz import collect_xAPI_fields

class ValidateBeginnerQuizForm(FormValidationAction):
    def __init__(self):
        self.score = 0

    def name(self) -> Text:
        return "validate_beginner_quiz_form"

    @staticmethod
    def beginnerQ1_db() -> List[Text]:
        """Database of supported responses for beginner Q1"""

        return ["2", "two"]

    @staticmethod
    def beginnerQ2_db() -> List[Text]:
        """Database of supported responses for beginner Q2"""

        return ["11", "eleven"]

    def common_validation(self, context, score):
        if context["given_answer"].lower() in context["possible_answers"]:
            self.score += 1
            context["result"] = True

            return {context["slot"]: context["given_answer"], "score": self.score, "answer_context": context}
        else:
            return {context["slot"]: context["given_answer"], "answer_context": context}

    def validate_beginnerQ1(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        context = {
            "actor_name": tracker.get_slot("name"),
            "activity_name": "Beginner Quiz: Question 1",
            "slot": "beginnerQ1",
            "activity_description": [d for d in tracker.events if d['event'] == 'bot'][-1]['text'],
            "possible_answers": self.beginnerQ1_db(),
            "given_answer": slot_value,
            "activity_type": "http://adlnet.gov/expapi/activities/cmi.interaction",
            "activity_interaction_type": "fill-in",
            "result": False
        }

        update = self.common_validation(context, self.score)

        collect_xAPI_fields(update["answer_context"])

        return update

    def validate_beginnerQ2(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        context = {
            "actor_name": tracker.get_slot("name"),
            "activity_name": "Beginner Quiz: Question 2",
            "slot": "beginnerQ2",
            "activity_description": [d for d in tracker.events if d['event'] == 'bot'][-1]['text'],
            "possible_answers": self.beginnerQ2_db(),
            "given_answer": slot_value,
            "activity_type": "http://adlnet.gov/expapi/activities/cmi.interaction",
            "activity_interaction_type": "fill-in",
            "result": False
        }

        update = self.common_validation(context, self.score)

        collect_xAPI_fields(update["answer_context"])

        return update
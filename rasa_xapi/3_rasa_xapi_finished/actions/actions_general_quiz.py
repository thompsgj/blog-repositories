from typing import Text, List, Any, Dict

from rasa_sdk import Action
from rasa_sdk.events import SlotSet, AllSlotsReset

from .xAPI_statement_builder import generate_xAPI_statement

def collect_xAPI_fields(context):
        statement_components = {
            # Actor
            "actor_name": context['actor_name'],
            "actor_mbox": "test@test.com",
            
            # Verb
            "verb_id": "http://adlnet.gov/expapi/verbs/answered",
            "verb_short": "answered",

            # Object
            "object_id": "https://chatbotdesign.substack.com/",
            "activity_name": context['activity_name'],
            "activity_description": context['activity_description'],

            # Result
            "result": context['result'],
            "given_answer": context['given_answer'],

            # Context
            "instructor_name": "Rasa Bash Quiz Bot",
            "instructor_mbox": "rasabashqzbt@test.com",

            # Dynamic components
            "activity_type": context['activity_type'],
            "activity_interaction_type": context['activity_interaction_type'],
            "possible_answers": context['possible_answers'],
        }

        generate_xAPI_statement(statement_components)


class ResetForms(Action):
    def name(self):
        return "reset_forms"

    def run(self, dispatcher, tracker, domain):
        name = tracker.get_slot('name')
        quiz_level = tracker.get_slot('quiz_level')
        score = tracker.get_slot('score')
        return [AllSlotsReset(), SlotSet("name", name), SlotSet("quiz_level", quiz_level), SlotSet("score", score)]


class ResetScore(Action):
    def name(self):
        return "reset_score"

    def run(self, dispatcher, tracker, domain):
        score = tracker.get_slot('score')
        quiz_retake = tracker.get_slot('quiz_retake')
        return[SlotSet("score", 0), SlotSet("quiz_retake", '')]

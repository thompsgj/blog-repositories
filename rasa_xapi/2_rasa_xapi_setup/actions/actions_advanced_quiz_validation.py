from typing import Text, List, Any, Dict

from rasa_sdk import Tracker, FormValidationAction
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.types import DomainDict

class ValidateAdvancedQuizForm(FormValidationAction):
    def __init__(self):
        self.score = 0

    def name(self) -> Text:
        return "validate_advanced_quiz_form"

    @staticmethod
    def advancedQ1_db() -> List[Text]:
        """Database of supported responses for advanced Q1"""

        return ["873", "eight hundred seventy-three"]

    @staticmethod
    def advancedQ2_db() -> List[Text]:
        """Database of supported responses for advanced Q2"""

        return ["444", "four hundred forty-four"]

    def common_validation(self, context, score):
        if context["given_answer"].lower() in context["possible_answers"]:
            self.score += 1

            return {context["slot"]: context["given_answer"], "score": self.score, "answer_context": context}
        else:
            return {context["slot"]: context["given_answer"], "answer_context": context}

    def validate_advancedQ1(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        context = {
            "slot": "advancedQ1",
            "possible_answers": self.advancedQ1_db(),
            "given_answer": slot_value,
        }

        update = self.common_validation(context, self.score)

        return update

    def validate_advancedQ2(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        context = {
            "slot": "advancedQ2",
            "possible_answers": self.advancedQ2_db(),
            "given_answer": slot_value,
        }

        update = self.common_validation(context, self.score)

        return update

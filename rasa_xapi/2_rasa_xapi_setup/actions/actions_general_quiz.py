from typing import Text, List, Any, Dict

from rasa_sdk import Action
from rasa_sdk.events import SlotSet, AllSlotsReset

class ResetForms(Action):
    def name(self):
        return "reset_forms"

    def run(self, dispatcher, tracker, domain):
        name = tracker.get_slot('name')
        quiz_level = tracker.get_slot('quiz_level')
        score = tracker.get_slot('score')
        return [AllSlotsReset(), SlotSet("name", name), SlotSet("quiz_level", quiz_level), SlotSet("score", score)]

class ResetScore(Action):
    def name(self):
        return "reset_score"

    def run(self, dispatcher, tracker, domain):
        score = tracker.get_slot('score')
        quiz_retake = tracker.get_slot('quiz_retake')
        return[SlotSet("score", 0), SlotSet("quiz_retake", '')]

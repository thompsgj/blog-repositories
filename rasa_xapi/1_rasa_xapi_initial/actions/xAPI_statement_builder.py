# An example script showing the functionality of the TinCanPython Library

import uuid

from tincan import lrs_properties
from tincan import (
    RemoteLRS,
    Statement,
    Agent,
    Verb,
    Activity,
    Context,
    LanguageMap,
    ActivityDefinition,
    Result
)

def generate_statement(statement_components):
    """ 
        {
            'actor_name': 'Chuck', Y
            'actor_mbox': 'test@test.com', Y
            'verb_id': 'http://adlnet.gov/expapi/verbs/answered', Y
            'verb_short': 'answered', Y
            'object_id': 'https://chatbotdesign.substack.com/', Y
            'activity_name': 'Beginner Quiz: beginnerQ1', Y
            'activity_description': 'What is 1 + 1?', Y
            'instructor_name': 'Rasa Bash Quiz Bot', Y
            'instructor_mbox': 'rasabashqzbt@test.com', Y
            'possible_answers': ['2', 'two'], 
            'given_answer': '2', 
            'activity_type': 'http://adlnet.gov/expapi/activities/cmi.interaction', Y
            'activity_interaction_type': 'fill-in',  Y
            'result': False  Y
        }
    """
    # construct an LRS
    print("constructing the LRS...")
    lrs = RemoteLRS(
        version=lrs_properties.version,
        endpoint=lrs_properties.endpoint,
        username=lrs_properties.username,
        password=lrs_properties.password,
    )
    print("LRS VALUES")
    print(lrs)
    print("...done")

    # construct the actor of the statement
    print("constructing the Actor...")
    actor = Agent(
        name=statement_components['actor_name'],
        mbox=statement_components['actor_mbox'],
    )
    print("...done")

    # construct the verb of the statement
    print("constructing the Verb...")
    verb = Verb(
        id=statement_components['verb_id'],
        display=LanguageMap({'en-US': statement_components['verb_short']}),
    )
    print("...done")

    # construct the object of the statement
    print("constructing the Object...")
    object = Activity(
        id=statement_components['object_id'],
        definition=ActivityDefinition(
            name=LanguageMap({'en-US': statement_components['activity_name']}),
            description=LanguageMap({'en-US': statement_components['activity_description']}),
            type = statement_components['activity_type'],
            interactionType = statement_components['activity_interaction_type'],
            correctResponsesPattern = statement_components['possible_answers']
        )
    )
    print("...done")


    print("constructing the Result")
    result = Result(
        success = statement_components['result'],
        response = statement_components['given_answer']
    )
    print("...done")

    # construct a context for the statement
    print("constructing the Context...")
    context = Context(
        registration=uuid.uuid4(),
        instructor=Agent(
            name=statement_components['instructor_name'],
            mbox=statement_components['instructor_mbox'],
        ),
        # language='en-US',
    )
    print("...done")

    # construct the actual statement
    print("constructing the Statement...")
    statement = Statement(
        actor=actor,
        verb=verb,
        object=object,
        result=result,
        context=context,
    )
    print("...done")

    # save our statement to the remote_lrs and store the response in 'response'
    print("saving the Statement...")
    response = lrs.save_statement(statement)

    if not response:
        raise ValueError("statement failed to save")
    print("...done")

from typing import Text, List, Any, Dict

from rasa_sdk import Tracker, FormValidationAction
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.types import DomainDict

class ValidateBeginnerQuizForm(FormValidationAction):
    def __init__(self):
        self.score = 0

    def name(self) -> Text:
        return "validate_beginner_quiz_form"

    @staticmethod
    def beginnerQ1_db() -> List[Text]:
        """Database of supported responses for beginner Q1"""

        return ["2", "two"]

    @staticmethod
    def beginnerQ2_db() -> List[Text]:
        """Database of supported responses for beginner Q2"""

        return ["11", "eleven"]

    def common_validation(self, context, score):
        if context["given_answer"].lower() in context["possible_answers"]:
            self.score += 1

            return {context["slot"]: context["given_answer"], "score": self.score, "answer_context": context}
        else:
            return {context["slot"]: context["given_answer"], "answer_context": context}

    def validate_beginnerQ1(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        context = {
            "slot": "beginnerQ1",
            "possible_answers": self.beginnerQ1_db(),
            "given_answer": slot_value,
        }

        update = self.common_validation(context, self.score)

        return update

    def validate_beginnerQ2(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        context = {
            "slot": "beginnerQ2",
            "possible_answers": self.beginnerQ2_db(),
            "given_answer": slot_value,
        }

        update = self.common_validation(context, self.score)

        return update
from django.urls import path
from .views import (
    chatroom,
    send_message
)

urlpatterns = [
    path("", chatroom, name='chatroom'),
    path("test", send_message, name='test')
]
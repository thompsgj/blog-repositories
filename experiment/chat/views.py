import pusher
from django.shortcuts import render
from django.http import JsonResponse

def chatroom(request):
    return render(request, 'chat/chat.html')

def send_message(request):
    if request.POST.get('method') == 'POST':
        message = request.POST.get('message')

    pusher_client = pusher.Pusher(
        app_id='{your-app_id}',
        key='{your-key}',
        secret='{your-secret}',
        cluster='ap3',
        ssl=True
    )

    pusher_client.trigger(
        'my-channel', 
        'my-event', 
        message
    )
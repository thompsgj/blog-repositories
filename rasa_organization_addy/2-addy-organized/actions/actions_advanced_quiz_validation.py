from typing import Text, List, Any, Dict

from rasa_sdk import Tracker, FormValidationAction, Action
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.types import DomainDict
from rasa_sdk.events import SlotSet, AllSlotsReset

class ValidateAdvancedQuizForm(FormValidationAction):
    def __init__(self):
        self.score = 0

    def name(self) -> Text:
        return "validate_advanced_quiz_form"

    @staticmethod
    def advancedQ1_db() -> List[Text]:
        """Database of supported responses for advanced Q1"""

        return ["873", "eight hundred seventy-three"]

    @staticmethod
    def advancedQ2_db() -> List[Text]:
        """Database of supported responses for advanced Q2"""

        return ["444", "four hundred forty-four"]

    def common_validation(self, slot, val, db, score):
        if val.lower() in db:
            self.score += 1

            return {slot: val, "score": self.score}
        else:
            return {slot: val}

    def validate_advancedQ1(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        db = self.advancedQ1_db()
        slot = "advancedQ1"
        update = self.common_validation(slot, slot_value, db, self.score)

        return update

    def validate_advancedQ2(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        db = self.advancedQ2_db()
        slot = "advancedQ2"
        update = self.common_validation(slot, slot_value, db, self.score)

        return update

from typing import Text, List, Any, Dict

from rasa_sdk import Tracker, FormValidationAction, Action
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.types import DomainDict
from rasa_sdk.events import SlotSet, AllSlotsReset

class ValidateBeginnerQuizForm(FormValidationAction):
    def __init__(self):
        self.score = 0

    def name(self) -> Text:
        return "validate_beginner_quiz_form"

    @staticmethod
    def beginnerQ1_db() -> List[Text]:
        """Database of supported responses for beginner Q1"""

        return ["2", "two"]

    @staticmethod
    def beginnerQ2_db() -> List[Text]:
        """Database of supported responses for beginner Q2"""

        return ["11", "eleven"]

    def common_validation(self, slot, val, db, score):
        if val.lower() in db:
            self.score += 1

            return {slot: val, "score": self.score}
        else:
            return {slot: val}

    def validate_beginnerQ1(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        db = self.beginnerQ1_db()
        slot = "beginnerQ1"
        update = self.common_validation(slot, slot_value, db, self.score)

        return update

    def validate_beginnerQ2(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        db = self.beginnerQ2_db()
        slot = "beginnerQ2"
        update = self.common_validation(slot, slot_value, db, self.score)

        return update